	<br>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?php echo form_open(base_url('config/users/')); ?>
			<table class="table table-hover">
				<thead>
					<tr>
		 				<th>Naam</th>
		  				<th class="text-center">Admin</th>
						<?php if ($this->session->superadmin == '1') { ?>
						<th class="text-center">Superadmin</th>
						<?php }; ?>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($users as $user) { ?>
					<tr>
		 				<td><?php echo $user['username']; ?></td>
		  				<td class="text-center"><?php echo form_checkbox('admin'.$user['id'], '1', $user['admin']); ?></td>
						<?php if ($this->session->superadmin == '1') { ?>
						<td class="text-center"><?php echo form_checkbox('superadmin'.$user['id'], '1', $user['superadmin']); ?></td>
						<?php }; ?>
						<td>
							<!-- ><span class="glyphicon glyphicon-cog"></span> -->
							<?php if ($this->session->superadmin != '1') { ?>
								<?php if ($user['superadmin'] == '1') { ?>
									<span class="glyphicon glyphicon-trash"></span>
								<?php } else { ?>
									<a href="<?php echo base_url('config/users/remove/'.$user['id'])?>"><span class="glyphicon glyphicon-trash text-danger"></span></a>
									<?php }; ?>
							<?php } else { ?>
								<a href="<?php echo base_url('config/users/remove/'.$user['id'])?>"><span class="glyphicon glyphicon-trash text-danger"></span></a>
							<?php }; ?>
						</td>
					</tr>
					<?php }; ?>
				</tbody>
			</table>
			<div class="text-right">
				<a class="btn btn-default" href="<?php echo base_url('config/users/add/')?>" role="button"><span class="glyphicon glyphicon-plus"></span></a>
				<button type="submit" class="btn btn-default" name="save" value="yes"><span class="glyphicon glyphicon-floppy-disk"></span></button>
				<br><br>
				<?php if (isset($updatemsg)) {?>
				<div class="alert alert-success" role="alert"><?php echo $updatemsg?></div>
				<?php }; ?>
			</div>
			</form>
		</div>
	</div>
</div>