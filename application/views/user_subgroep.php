<div class="container">
	<hr>
	<div class='text-center'>
		<div class="btn-group" role="group" aria-label="...">
			<a href="<?php echo base_url('beoordelen/lijst'); ?>" role="button" class="btn btn-success">Beoordeling</span></a>
			<a href="<?php echo base_url('beoordelen/ronde'); ?>" role="button" class="btn btn-success">Ronde</button></a>
			<?php if (count($subgroepen) == 0) { ?>
			<a href="<?php echo base_url('beoordelen/subgroep'); ?>" role="button" class="btn btn-success">Ploeg</button></a>
			<button type="button" class="btn btn-success">Score</button>
			<?php } else { ?>
			<a href="<?php echo base_url('beoordelen/subgroep'); ?>" role="button" class="btn btn-warning">Ploeg</button></a>
			<button type="button" class="btn btn-default">Score</button>
			<?php } ?>
		</div>
	</div>

	<?php if (count($subgroepen) != 0) { ?>
	
	<?php echo form_open(base_url().'beoordelen/score'); ?>
	
	<p>
		<div class="row">
			<div class="text-center">
				<?php echo form_dropdown('subgroepid', $subgroepen); ?>
			</div>
		</div>
	</p>
	<p>
		<div class="row">
			<div class="text-center">
				<a href="<?php echo base_url('beoordelen/ronde'); ?>" type="submit" class="btn btn-primary btn-lg">Vorige</button></a>
				<button type="submit" class="btn btn-primary btn-lg">Volgende</button>
			</div>
		</div>
	</p>
	
	<?php echo form_close(); ?>
		  
	<?php } else { ?>
	
	<p>
		<div class="row">
			<div class="text-center">
				Er zijn geen groepen meer te beoordelen.
			</div>
		</div>
	</p>
	<p>
		<div class="row">
			<div class="text-center">
				<button type="button" class="btn btn-success btn-lg" disabled="disabled">
					<span class="glyphicon glyphicon-ok"></span>
			  	</button>
			</div>
		</div>
	</p>
	
	<?php } ?>
		  
</div>