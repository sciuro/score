<div class="container">
	<hr>
	<div class='text-center'>
		<div class="btn-group" role="group" aria-label="...">
			<a href="<?php echo base_url('beoordelen/lijst'); ?>" role="button" class="btn btn-success">Beoordeling</span></a>
			<a href="<?php echo base_url('beoordelen/ronde'); ?>" role="button" class="btn btn-warning">Ronde</button></a>
			<button type="button" class="btn btn-default">Ploeg</button>
			<button type="button" class="btn btn-default">Score</button>
		</div>
	</div>
	
	<?php echo form_open(base_url().'beoordelen/subgroep')."\n"; ?>

	<p>
		<div class="row">
			<div class="text-center">
				<label>Ronde</label>
				<?php echo form_dropdown('ronde', $ronde); ?>
			</div>
		</div>
	</p>
	<p>
		<div class="row">
			<div class="text-center">
				<a href="<?php echo base_url('beoordelen/lijst'); ?>" type="submit" class="btn btn-primary btn-lg">Vorige</button></a>
				<button type="submit" class="btn btn-primary btn-lg">Volgende</button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</p>
</div>