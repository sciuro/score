<div class="container">
	<hr>
	<div class="row">
		<div class="col-md-12">
			<h3>Lijst: <?php echo $lijstnaam; ?><br>Ronde: <?php echo $ronde; ?></h4>

			<?php echo form_open(base_url().'edit/save'); ?>
			<?php echo form_hidden('lijstid', $lijstid); ?>
			<?php echo form_hidden('ronde', $ronde); ?>
			
			<table class="table table-hover" id="maintable">
				<thead>
					<tr>
						<th>#</th>
						<th>Naam</th>
						<?php foreach ($vragen as $vraag) { ?>
						<th><?php echo $vraag['vraag']; ?></th>
						<?php } ?>
					</tr>
				</thead>
				
				<tbody>
					<?php foreach ($subgroepen as $subgroep) {?>
					<tr>
						<td><?php echo $subgroep['nummer']; ?></td>
						<td><?php echo $subgroep['themanaam']; ?></td>
						<?php foreach ($vragen as $vraag) { ?>
						<td>
						
							<?php
							if ($vraag['antwoord'] == 0) {
								$checkbox = array(
									'name' => $vraag['id'].'-'.$subgroep['id'],
									'value' => $vraag['score']
								);
								if (isset($scores[$subgroep['id']][$vraag['id']]['score']) && $scores[$subgroep['id']][$vraag['id']]['score'] == $vraag['score']) {
									$checkbox['checked'] = TRUE;
								} else {
									$checkbox['checked'] = FALSE;
								}
								echo form_checkbox($checkbox);
							} elseif ($vraag['antwoord'] == 1) {
								$dropdown = array();
								for ($i=0; $i <= $vraag['score']; $i++) {
									$dropdown[$i] = $i;
								}
								if (isset($scores[$subgroep['id']][$vraag['id']]['score'])) {
									$select=$scores[$subgroep['id']][$vraag['id']]['score'];
								} else {
									$select=0;
								}
									echo form_dropdown($vraag['id'].'-'.$subgroep['id'], $dropdown, $select);
							} elseif ($vraag['antwoord'] == 2) {
								if (isset($scores[$subgroep['id']][$vraag['id']]['score'])) {
									$select=$scores[$subgroep['id']][$vraag['id']]['score'];
								} else {
									$select="0.00";
								}
								
								$inputsetting = array(
								        'name'          => $vraag['id'].'-'.$subgroep['id'],
								        'value'         => $select,
								        'maxlength'     => '4',
								        'size'          => '4'
								);
								echo form_input($inputsetting);
							}
							?>
						
						</td>
						<?php } ?>
					</tr>
					<?php } ?>
				</tbody>

			</table>
			<div class="text-right">
				<button type="submit" class="btn btn-default" name="save" value="yes"><span class="glyphicon glyphicon-floppy-disk text-danger"></span></button>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div
</div>