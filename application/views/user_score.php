<div class="container">
	<hr>
	<div class='text-center'>
		<div class="btn-group" role="group" aria-label="...">
			<a href="<?php echo base_url('beoordelen/lijst'); ?>" role="button" class="btn btn-success">Beoordeling</span></a>
			<a href="<?php echo base_url('beoordelen/ronde'); ?>" role="button" class="btn btn-success">Ronde</button></a>
			<a href="<?php echo base_url('beoordelen/subgroep'); ?>" role="button" class="btn btn-success">Ploeg</button></a>
			<button type="button" class="btn btn-warning">Score</button>
		</div>
	</div>


	<?php echo form_open(base_url().'beoordelen/score_save'); ?>
	<?php echo form_hidden('subgroepid', $subgroepid); ?>
	
	<p>
		<?php
		foreach ($vragen as $vraag)
		{ 
			$score = array();
			?>
		<div class="row">
			<div class="col-xs-9 col-sm-6 col-sm-offset-3">
				<?php echo $vraag['vraag']; ?>
			</div>
			<div class="col-xs-3 col-sm-1">
				<?php if ($vraag['antwoord'] == 0) {
					echo ': '.form_checkbox($vraag['id'], '1');
				} elseif ($vraag['antwoord'] == 1) {
					for ($i=0; $i <= $vraag['score']; $i++) {
						$score[$i] = $i;
					} 
					echo form_dropdown($vraag['id'], $score);
				} elseif ($vraag['antwoord'] == 2)  {
					$inputsetting = array(
					        'name'          => $vraag['id'],
					        'value'         => '0',
					        'maxlength'     => '4',
					        'size'          => '4'
					);
					echo ': '.form_input($inputsetting);
				} ?>					  
			</div>
		</div>
		<?php } ?>
	</p>
	
	<p>
		<div class="row">
			<div class="text-center">
				<a href="<?php echo base_url('beoordelen/subgroep'); ?>" type="submit" class="btn btn-primary btn-lg">Vorige</button></a>
				<button type="submit" class="btn btn-danger btn-lg">Opslaan</button>
			</div>
		</div>
	</p>
	
	<?php echo form_close(); ?>

</div>
