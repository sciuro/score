<div class="container">
	<hr>
	<p><b>Totalen</b><br>
		<!--
		Er is op dit moment <?php echo $gedaan; ?>% van de beoordelingen beantwoord.</p>
	<div class="progress">
	  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $gedaan; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $gedaan; ?>%">
	    <span class="sr-only"><?php echo $gedaan; ?>% Gedaan</span>
	  </div>
	</div>
		-->
	
		Er is op dit moment van de lijsten <?php echo $bar['gedaan']; ?>% helemaal gedaan,
		<?php echo $bar['bezig']; ?>% is gedeeltelijk gedaan en
		<?php echo $bar['doen']; ?>% moet nog gedaan worden.</p>
	<div class="progress">
	  <div class="progress-bar progress-bar-success" style="width: <?php echo $bar['gedaan']; ?>%">
	    <span class="sr-only"><?php echo $bar['gedaan']; ?>% Gedaan</span>
	  </div>
	  <div class="progress-bar progress-bar-warning" style="width: <?php echo $bar['bezig']; ?>%">
	    <span class="sr-only"><?php echo $bar['bezig']; ?>% Nog bezig</span>
	  </div>
	</div>

	
	<p><b>Per onderdeel</b>
	<?php foreach ($bardata as $bar) { ?>
		
		<p><?php echo $bar['naam']; ?>
		<div class="progress">
		  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $bar['percentage']; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $bar['percentage']; ?>%">
		    <span class="sr-only"><?php echo $bar['percentage']; ?>% Complete</span>
		  </div>
		</div>
		</p>
	<?php } ?>
	</p>
	
</div>