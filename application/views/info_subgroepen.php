<div class="container">
	<hr>

	<?php if (count($subgroepen) != 0) { ?>
	
	<?php echo form_open(base_url().'info/subgroep'); ?>
	
	<p>
		<div class="row">
			<div class="text-center">
				<?php echo form_dropdown('subgroepid', $subgroepen); ?>
			</div>
		</div>
	</p>
	<p>
		<div class="row">
			<div class="text-center">
				<button type="submit" class="btn btn-primary btn-lg">Geef info</button>
			</div>
		</div>
	</p>
	
	<?php echo form_close(); ?>
		  
	<?php } else { ?>
	
	<p>
		<div class="row">
			<div class="text-center">
				Er zijn geen groepen te vinden.
			</div>
		</div>
	</p>
	<p>
		<div class="row">
			<div class="text-center">
				<button type="button" class="btn btn-error btn-lg" disabled="disabled">
					<span class="glyphicon glyphicon-ok"></span>
			  	</button>
			</div>
		</div>
	</p>
	
	<?php } ?>
		  
</div>