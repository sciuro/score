<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Score systeem</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<?php $burl=base_url('/');
	if ($this->session->admin == "1") {$functie = "admin";} else {$functie="user";};
	if (($burl=="http://jury.scoutingutrecht.nl/") && ($this->session->regionaam != "") && ($this->session->superadmin != "1")) { ?>
	<script type="text/javascript">
	  var _paq = _paq || [];
	  _paq.push(["setDomains", ["*.jury.scoutingutrecht.nl"]]);
	  _paq.push(["setCustomVariable", 1, "regio", "<?php echo $this->session->regionaam; ?>", "visit"]);
	  _paq.push(["setCustomVariable", 2, "functie", "<?php echo $functie; ?>", "visit"]);
	  _paq.push(["setCustomVariable", 3, "user", "<?php echo $this->session->id; ?>", "visit"]);
	  _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
	    var u="//analytics.sciuro.org/";
	    _paq.push(['setTrackerUrl', u+'piwik.php']);
	    _paq.push(['setSiteId', 8]);
	    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	  })();
	</script>
	<noscript><p><img src="//analytics.sciuro.org/piwik.php?idsite=8" style="border:0;" alt="" /></p></noscript>
	<?php } ?>
	
</head>
<body>

<?php if($this->session->userdata('validated')) { ?>
<div class="container">
	<div class="header clearfix">
		<nav>
			<div class="navbar-text"><?php echo $this->session->regionaam; ?></div>
			<ul class="nav nav-pills pull-right">
				<li role="presentation" <?php if ($page == 'home') {?> class="active" <?php } ?> ><a href="<?php echo base_url('/');?>">Home</a></li>
				<li role="presentation" <?php if ($page == 'beoordelen') {?> class="active" <?php } ?> ><a href="<?php echo base_url('beoordelen/');?>">Beoordelen</a></li>
				<?php if ($this->session->admin == '1' OR $this->session->superadmin == '1') { ?>
					<li role="presentation" <?php if ($page == 'uitslag') {?> class="active" <?php } ?> ><a href="<?php echo base_url('uitslag/');?>">Uitslag</a></li>
					<li role="presentation" <?php if ($page == 'edit') {?> class="active" <?php } ?> ><a href="<?php echo base_url('edit/');?>">Bewerken</a></li>
				<?php }; ?>
				<li role="presentation" <?php if ($page == 'info') {?> class="active" <?php } ?> ><a href="<?php echo base_url('info/');?>">Informatie</a></li>
				<?php if ($this->session->admin == '1' OR $this->session->superadmin == '1') { ?>
				<li role="presentation" <?php if ($page == 'config') {?> class="active" <?php } ?> ><a href="<?php echo base_url('config/');?>">Configuratie</a></li>
				<?php }; ?>
				<?php if ($this->session->superadmin == '1') { ?>
					<li role="presentation" <?php if ($page == 'regio') {?> class="active" <?php } ?> ><a href="<?php echo base_url('dashboard/changeregio/');?>">Regio </a></li>
				<?php }; ?>
				<li role="presentation"><a href="<?php echo base_url('logout/');?>">Logout</a></li>
			</ul>
		</nav>
	</div>
</div>
<?php } ?>