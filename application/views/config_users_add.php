	<br>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?php echo form_open(base_url('config/users/'), 'class="form-horizontal"'); ?>
			
			<div class='form-group'>
				<label for="inputUsername" class="col-sm-4 control-label">Inlognaam</label>
				<div class='col-sm-8'>
					<?php echo form_input('username', '', 'class="form-control"');	?>
				</div>
			</div>
			
			<div class='form-group'>
				<label for="inputPassword" class="col-sm-4 control-label">Wachtwoord</label>
				<div class='col-sm-8'>
					<?php echo form_password('password', '', 'class="form-control"');	?>
				</div>
			</div>
			
			<div class='form-group'>
				<label for="inputAdmin" class="col-sm-4 control-label">Admin</label>
				<div class='col-sm-8'>
					<div class="checkbox"><?php echo form_checkbox('admin', '1', '0'); ?></div>
				</div>
			</div>
			
			<?php if ($this->session->superadmin == '1') { ?>
			<div class='form-group'>
				<label for="inputSuperAdmin" class="col-sm-4 control-label">Superadmin</label>
				<div class='col-sm-8'>
					<div class="checkbox"><?php echo form_checkbox('superadmin', '1', '0'); ?></div>
				</div>
			</div>
			<?php }; ?>
			
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-8 text-right">
					<a class="btn btn-default" href="<?php echo base_url('config/users/')?>" role="button"><span class="glyphicon glyphicon-remove"></span></a>
					<button type="submit" class="btn btn-default" name="saveuser" value="yes"><span class="glyphicon glyphicon-floppy-disk"></span></button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>