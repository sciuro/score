<div class='container-fluid'>
	<div class='row'>
		<div class="ol-md-6 col-md-offset-3 .hidden-sm">
			<img class="img-responsive" src="<?php echo base_url('images/login1.jpg'); ?>">
		</div>
	</div>
</div>

<div class='container-fluid'>
	<div class='row'>
		<div class='col-xs-12 col-md-4 col-md-offset-4'>
		<?php
		// Viewing error's
		if (isset($errormsg)) { ?>
			<div class="alert alert-danger" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				<span class="sr-only">Error:</span>
				<?php echo $errormsg; ?>
			</div>
		<?php } ?>
		</div>
	</div>
	
	<div class='row'>		
		<div class='col-md-2 col-md-offset-5'>
			<?php echo form_open(base_url('login/'), 'class="form-horizontal"'); ?>
				<div class='form-group'>
					<label for="inputUsername" class="col-sm-4 control-label">Username</label>
					<div class='col-sm-8'>
						<?php echo form_input('username', '', 'class="form-control"');	?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword" class="col-sm-4 control-label">Password</label>
					<div class="col-sm-8">
						<?php echo form_password('password', '', 'class="form-control"'); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputRegio" class="col-sm-4 control-label">Regio</label>
					<div class="col-sm-8">
						<?php echo form_dropdown('regioid', $regio, '1', 'class="form-control"'); ?>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-8">
						<button type="submit" class="btn btn-default">Sign in</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>