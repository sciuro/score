<div class="container">
	<hr>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<?php $total=0; $totalmax=0; ?>
			<p>
			<h3><?php echo $subgroepinfo['themanaam']; ?></h3><br>
			<?php echo $subgroepinfo['groepsnaam'].', '.$subgroepinfo['plaats']; ?>
			</p>
			<table class="table table-condensed ">
				<thead>
					<tr>
						<th>Spelgebied</th>
						<th>Onderdeel</th>
						<th>Score</th>
						<th>Maximaal</th>
						<th>Percentage</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($scores as $spelgebied) { ?>
					<tr>
						<td><strong><?php echo $spelgebied['naam']; ?></strong></td>
						<?php $scoretot=0;
						$scoremaxtot=0;
						foreach ($spelgebied['onderdeel'] as $onderdeel) { ?>
							<td><?php echo $onderdeel['naam']; ?></td>
							<td class="text-right"><?php echo number_format(round($onderdeel['score'], $round), $round, ',', ''); $scoretot = $scoretot + $onderdeel['score']; ?></td>
							<td class="text-right"><?php echo number_format(round($onderdeel['scoremax'],$round), $round); $scoremaxtot = $scoremaxtot + $onderdeel['scoremax']; ?></td>
							<td class="text-right"><?php echo number_format(round(($onderdeel['score']/$onderdeel['scoremax'])*100, $round), $round, ',', '')."%"; ?></td>
						</tr>
						<tr>
							<td></td>

						<?php $total=$total+$onderdeel['score'];
						$totalmax=$totalmax+$onderdeel['scoremax'];
						} ?>
						<td class="active"><strong>Subtotaal:</strong></td>
						<td class="text-right active"><strong><?php echo number_format(round($scoretot, $round), $round, ',', ''); ?></strong></td>
						<td class="text-right active"><strong><?php echo number_format(round($scoremaxtot, $round), $round, ',', ''); ?></strong></td>
						<td class="text-right active"><strong><?php echo number_format(round(($scoretot/$scoremaxtot)*100, $round), $round, ',', ''); ?>%</strong></td>
					</tr>
					<?php } ?>
				</tbody>
				<thead>
					<tr class="active">
						<th></th>
						<th>Totaal:</th>
						<th class="text-right"><?php echo number_format(round($total, $round), $round, ',', ''); ?></th>
						<th class="text-right"><?php echo number_format(round($totalmax, $round), $round, ',', ''); ?></th>
						<th class="text-right"><?php echo number_format(round(($total/$totalmax)*100, $round), $round, ',', ''); ?>%</th>
					</tr>
				</thead>
			</table>
		</div>
	</div
</div>