<div class="container">
	<hr>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<table class="table table-condensed ">
				<tr>
					<td class="text-right"><b>Nummer</b></td>
					<td><?php echo $subgroepinfo->nummer; ?></td>
				</tr>
				<tr>
					<td class="text-right"><b>Themanaam</b></td>
					<td><?php echo $subgroepinfo->themanaam; ?></td>
				</tr>
				<tr>
					<td class="text-right"><b>Naam</b></td>
					<td><?php echo $subgroepinfo->naam; ?></td>
				</tr>
				<tr>
					<td class="text-right"><b>Groepsnaam</b></td>
					<td><?php echo $subgroepinfo->groepsnaam; ?></td>
				</tr>
				<tr>
					<td class="text-right"><b>Plaats</b></td>
					<td><?php echo $subgroepinfo->plaats; ?></td>
				</tr>
			</table>
		</div>
	</div
</div>