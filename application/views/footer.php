<div class="container">
	<hr>
	
	<footer>
		<p>&copy; Jury Ondersteunings Stam 2016</p>
	</footer>
	
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('bootstrap/js/bootstrap.min.js'); ?>"></script>
	
	<?php if (isset($page) && $page == 'uitslag') { ?>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/t/bs-3.3.6/dt-1.10.11,af-2.1.1,fc-3.2.1/datatables.min.css"/>
		<script type="text/javascript" src="https://cdn.datatables.net/t/bs-3.3.6/dt-1.10.11,af-2.1.1,fc-3.2.1/datatables.min.js"></script>

		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#maintable').dataTable( {
					"paging":   false,
					"order": [[ 1, "asc" ]],
					"info":     false,
					"columnDefs" : [ {
						"targets": [ 4 ],
						"visible": false,
						"searchable": true
					} ]
				} );
			} );
		</script>
	<?php } elseif (isset($page) && $page == 'edit') { ?>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/t/bs-3.3.6/dt-1.10.11,af-2.1.1,fc-3.2.1/datatables.min.css"/>
		<script type="text/javascript" src="https://cdn.datatables.net/t/bs-3.3.6/dt-1.10.11,af-2.1.1,fc-3.2.1/datatables.min.js"></script>
		
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#maintable').dataTable( {
					"paging":   false,
					"ordering": false,
					"info":     false,
					"searching": true,
				} );
			} );
		</script>
	<?php } ?>
	
	</body>
</html>
