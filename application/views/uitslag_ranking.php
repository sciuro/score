<div class="container">
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover" id="maintable">
				<thead>
					<tr>
						<th>#</th>
						<th>Nr</th>
						<th>Themanaam</th>
						<th>Groep</th>
						<th>Plaats</th>
						<?php foreach ($spelgebieden as $spelgebied) { 
							echo "<th>".$spelgebied['naam']."</th>";
						} ?>
						<th>Totaal</th>
				</thead>
				
				<tbody>
					<?php foreach ($subgroepen as $subgroep) { ?>
						<tr>
							<td><?php echo $uitslag[$subgroep['id']]['rang']?></td>
							<td><?php echo $subgroep['nummer']?></td>
							<td><a href="<?php echo base_url('/uitslag/groep/'.$subgroep['id']); ?>"><?php echo $subgroep['themanaam']?></a></td>
							<td><?php echo $subgroep['groepsnaam']?></td>
							<td><?php echo $subgroep['plaats']?></td>
							<?php foreach ($spelgebieden as $spelgebied) {
								if (isset($uitslag[$subgroep['id']][$spelgebied['id']])) {
									$score = $uitslag[$subgroep['id']][$spelgebied['id']];
								} else {
									$score = 0;
								}
								echo "<td class='text-right'>".number_format(round($score, $round), $round, ',', '')."</td>";
							}
							echo "<td class='text-right'>".number_format(round($uitslag[$subgroep['id']]['total'], $round), $round, ',', '')."</td>";
							?>
						</tr>
					<?php } ?>
				</tbody>

			</table>
		</div>
	</div
</div>