<div class="container">
	<hr>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<p>
				Met deze pagina kun je lijsten actief of inactief maken tbv. de beoordeling.
				Tevens kunnen de ingevulde scores per ronde aangepast worden.
			</p>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Naam</th>
						<th>Ronde</th>
						<th>Actief</th>
				</thead>
				
				<tbody>
					<?php foreach ($lijsten as $lijst) {?>
					<tr>
						<td><?php echo $lijst['naam']; ?></td>
						<td class="text-center">
							<?php for ($i=1; $i<=$lijst['ronde']; $i++) { ?>
							<a href="<?php echo base_url('edit/lijst/'.$lijst['id'].'/'.$i)?>"><span class="glyphicon glyphicon-pencil"></span></a>
							<?php } ?>
						</td>
						<?php if ($lijst['actief'] == 1)  { ?>
						<td class="text-center"><a href="<?php echo base_url('edit/disable/'.$lijst['id'])?>"><span class="glyphicon glyphicon-ok text-success"></span></a></td>
						<?php } else { ?>
						<td class="text-center"><a href="<?php echo base_url('edit/enable/'.$lijst['id'])?>"><span class="glyphicon glyphicon-remove text-danger"></span></a></td>
						<?php } ?>
					</tr>
					<?php } ?>
				</tbody>

			</table>
		</div>
	</div
</div>