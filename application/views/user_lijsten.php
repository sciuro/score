<div class="container">
	<hr>
	<div class='text-center'>
		<div class="btn-group" role="group" aria-label="...">
			<a href="<?php echo base_url('beoordelen/lijst'); ?>" role="button" class="btn btn-warning">Beoordeling</span></a>
			<button type="button" class="btn btn-default">Ronde</button>
			<button type="button" class="btn btn-default">Ploeg</button>
			<button type="button" class="btn btn-default">Score</button>
		</div>
	</div>
	
	<?php if (isset($lijsten)) { ?>
	<?php echo form_open(base_url().'beoordelen/ronde'); ?>
	<p>
		<div class="row">
			<div class="text-center">
				<?php echo form_dropdown('lijstid', $lijsten); ?>
			</div>
		</div>
	</p>
	<p>
		<div class="row">
			<div class="text-center">
				<button type="submit" class="btn btn-primary btn-lg">Kies</button>
			</div>
		</div>
	</p>
	<?php echo form_close(); ?>
	
	<?php } else { ?>
	<p>
		<div class="row">
			<div class="text-center">
				Geen openstaande lijsten beschikbaar.
			</div>
		</div>
	</p>
	<?php } ?>
	
</div>
