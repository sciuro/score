<div class='container'>
	<hr>
	<?php echo form_open(base_url().'dashboard/changeregio'); ?>
	
	<p>
		<div class="row">
			<div class="text-center">
				<?php echo form_dropdown('regioid', $regio); ?>
			</div>
		</div>
	</p>
	
	<p>
		<div class="row">
			<div class="text-center">
				<button type="submit" class="btn btn-primary btn-lg">Kies</button>
			</div>
		</div>
	</p>
	
	<?php echo form_close(); ?>

</div>