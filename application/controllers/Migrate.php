<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Migrate to next database release
*/
class Migrate extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('migration');
	}
	
	public function version($id = NULL)
	{
		if ($this->migration->version($id) === FALSE)
		{
			show_error($this->migration->error_string());
		} else {
			echo "Database moved to: ".$id;
		}
	}
	
	public function latest()
	{
		if ($this->migration->latest() === FALSE)
		{
			show_error($this->migration->error_string());
		} else {
			echo "Database updated or update not needed.";
		}
	}
}

?>