<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Info Class
*/
class Config extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        if(! $this->session->userdata('validated')){
            redirect(base_url('/login'));
        }
		if (! ($this->session->admin == '1' OR $this->session->superadmin == '1')) {
			redirect(base_url('/dashboard'));
		}
	}
	
	public function index()
	{
		$data['page'] = 'config';
		$data['tab'] = 'common';
		
		// Header
		$this->load->view('header', $data);
		
		// Tab bar
		$this->load->view('config_tabbar', $data);
		
		// config page
		$this->load->view('config_common', $data);
		
		// Footer
		$this->load->view('footer');
	}
	
	public function users($action = NULL, $id = NULL)
	{
		$data['page'] = 'config';
		$data['tab'] = 'users';

		// Load data
		$this->load->model('Config_model');
				
		// Check for post
		if ($this->input->post('save')) {
			$users = $this->Config_model->get_user_list();
			foreach ($users as $user) {
				if ($this->input->post('admin'.$user['id']) == 1) {
					$update[$user['id']]['admin'] = 1;
				} else {
					$update[$user['id']]['admin'] = 0;
				}
				if ($this->input->post('superadmin'.$user['id']) == 1) {
					$update[$user['id']]['superadmin'] = 1;
				} else {
					$update[$user['id']]['superadmin'] = 0;
				}
			}
			// Update user
			$this->Config_model->update_user_rights($update);
			$data['updatemsg'] = 'Rechten aangepast.';
			
		} elseif ($this->input->post('saveuser')) {
			if ($this->input->post('username')) {
				$update['username'] = $this->input->post('username');
			}
			if ($this->input->post('password')) {
				$update['password'] = sha1($this->input->post('password'));
			}			
			if ($this->input->post('admin') == 1) {
				$update['admin'] = 1;
			} else {
				$update['admin'] = 0;
			}
			if ($this->input->post('superadmin') == 1) {
				$update['superadmin'] = 1;
			} else {
				$update['superadmin'] = 0;
			}
			$update['regioid'] = $this->session->regio;
			$this->Config_model->add_user($update);
			$data['updatemsg'] = 'Gebruiker aangemaakt.';
		}
						
		// Get data
		$data['users'] = $this->Config_model->get_user_list();
			
		// Header
		$this->load->view('header', $data);
		
		// Tab bar
		$this->load->view('config_tabbar', $data);
		
		// If add user
		if (isset($action)) {
			if ($action == 'add') {
				$this->load->view('config_users_add', $data);
			} elseif ($action == 'remove') {
				if (isset($id)) {
					$this->Config_model->remove_user($id);
					$data['users'] = $this->Config_model->get_user_list();
					$this->load->view('config_users', $data);
				}
			} else {
				$this->load->view('config_users', $data);
			}
		} else {
			// config page
			$this->load->view('config_users', $data);
		}
		
		//echo "<pre>";
		//print_r($update);
		//echo "</pre>";
		
		// Footer
		$this->load->view('footer');
	}
}