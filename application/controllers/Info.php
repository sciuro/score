<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Info Class
*/
class Info extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        if(! $this->session->userdata('validated')){
            redirect(base_url('/login'));
        }
	}
	
	public function index()
	{
		$data['page'] = 'info';
		
		// Get data
		$this->load->model('Info_model');
		$subgroepen = $this->Info_model->get_subgroep_list();
		
		// Prepare data
		$data['subgroepen'] = array();
		foreach ($subgroepen as $subgroep)
		{
			$data['subgroepen'][$subgroep['id']] = $subgroep['nummer'].': '.$subgroep['themanaam'];
		}
		
		// Header
		$this->load->view('header', $data);
		
		// login page
		$this->load->view('info_subgroepen', $data);
		
		// Footer
		$this->load->view('footer');
	}
	
	public function subgroep()
	{
		$data['page'] = 'info';
		
		$subgroepid = $this->security->xss_clean($this->input->post('subgroepid'));
		
		// Get data
		$this->load->model('Info_model');
		$data['subgroepinfo'] = $this->Info_model->get_subgroep_info($subgroepid);
		
		// Header
		$this->load->view('header', $data);
		
		//echo "<pre>";
		//print_r($data);
		//echo "<pre>";
		
		// login page
		$this->load->view('info_subgroep', $data);
		
		// Footer
		$this->load->view('footer');
	}
	
}