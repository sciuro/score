<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Login Class
*/
class Login extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function	index()
	{
		// redirect if session exists
		if($this->session->userdata('validated')){
			redirect(base_url('/dashboard'));
		}
				
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if (! ($this->input->post('username') AND $this->input->post('password')))
		{
			$this->_showlogin();
		}
		elseif ($this->form_validation->run() == FALSE)
		{
			$this->_showlogin('No username or password.');
		} else {
			// check login
			$this->_checklogin();
		}
				
	}
	
	private	function _showlogin($errormsg = NULL)
	{
		// Load model
		$this->load->model('Regio_model');
		$regios = $this->Regio_model->get_regio_list();
		
		// Arrange data
		foreach ($regios as $regio) {
			$data['regio'][$regio['id']] = $regio['naam'];
		}
		
		if ($errormsg)
		{
			$data['errormsg'] = $errormsg;
		}
		
		// Header
		$this->load->view('header');
		
		// login page
		$this->load->view('login', $data);
		
		// Footer
		$this->load->view('footer');
	}
	
	private function _checklogin()
	{
		// Load model
		$this->load->model('Login_model');
		$userdata = $this->Login_model->check_user();
		
		if ($userdata == FALSE) {
			$this->_showlogin('Wrong username or password');
		} else {
			$this->_startsession($userdata);
		}
		
	}
	
	private	function _startsession($userdata)
	{
		// Start session with user data
		$this->session->set_userdata($userdata);
		
		// Redirect to dashboard
		redirect(base_url('/dashboard'));
	}
}
	
?>