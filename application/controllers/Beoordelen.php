<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Logout Class
*/
class Beoordelen extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        if(! $this->session->userdata('validated')){
            redirect(base_url('/login'));
        }
		$data['page'] = 'beoordelen';
	}
	
	public function index()
	{
		redirect(base_url('/beoordelen/lijst'));
	}
	
	public function	lijst()
	{
		$data['page'] = 'beoordelen';
		
		// Get data
		$this->load->model('Beoordelen_model');
		$lijsten = $this->Beoordelen_model->get_lijst();
		
		// Data prepare
		foreach ($lijsten as $lijst)
		{
			$data['lijsten'][$lijst['id']] = $lijst['naam'];
		}
		
		// Header
		$this->load->view('header', $data);
		
		// login page
		$this->load->view('user_lijsten', $data);
		
		// Footer
		$this->load->view('footer');
	}
	
	public function	ronde()
	{
		$data['page'] = 'beoordelen';
		
		// Check input
		$lijstid = $this->security->xss_clean($this->input->post('lijstid'));
		if ($lijstid OR $this->session->userdata('lijstid')) {
			if ($lijstid) {
				$this->session->set_userdata('lijstid', $lijstid);
			} else {
				$lijstid = $this->session->userdata('lijstid');
			}
		} else {
			redirect(base_url('/beoordelen/lijst'));
		}
		
		// Check is ronde question is possible
		$this->load->model('Beoordelen_model');
		$rondes = $this->Beoordelen_model->get_rondes($lijstid)->ronde;
		
		if ($rondes == 1) {
			$this->session->set_userdata('ronde', '1');
			redirect(base_url('/beoordelen/subgroep'));
		}
		
		for ($i=1; $i <= $rondes; $i++) { 
			$data['ronde'][$i] = $i;
		}
		// Make page
		
		// Header
		$this->load->view('header', $data);
		
		// login page
		$this->load->view('user_ronde', $data);
		
		// Footer
		$this->load->view('footer');
		
	}
	
	public function subgroep()
	{
		$data['page'] = 'beoordelen';
		
		// Check input
		$ronde = $this->security->xss_clean($this->input->post('ronde'));
		if ($ronde OR $this->session->userdata('ronde')) {
			if ($ronde) {
				$this->session->set_userdata('ronde', $ronde);
			} else {
				$ronde = $this->session->userdata('ronde');
			}
		} else {
			redirect(base_url('/beoordelen/ronde'));
		}
		
		// Get groepdata
		$this->load->model('Beoordelen_model');
		$subgroepen = $this->Beoordelen_model->get_subgroepen();
		
		// Prepare data
		$data['subgroepen'] = array();
		foreach ($subgroepen as $subgroep)
		{
			if (!$this->Beoordelen_model->check_score($this->session->userdata('ronde'), $this->session->userdata('lijstid'), $subgroep['id'])) {
				$data['subgroepen'][$subgroep['id']] = $subgroep['nummer'].'. '.$subgroep['themanaam'];
			}
		}
		
		// Header
		$this->load->view('header', $data);
		
		// login page
		$this->load->view('user_subgroep', $data);
		
		// Footer
		$this->load->view('footer');
	}
	
	public function score()
	{
		$data['page'] = 'beoordelen';
		
		// Check input
		$subgroepid = $this->security->xss_clean($this->input->post('subgroepid'));
		if (!$subgroepid) {
			redirect(base_url('/beoordelen/subgroep'));
		}
		
		$data['subgroepid'] = $subgroepid;
		
		// Get questions
		$this->load->model('Beoordelen_model');
		$data['vragen'] = $this->Beoordelen_model->get_vragen();
		
		// Header
		$this->load->view('header', $data);
		
		// login page
		$this->load->view('user_score', $data);
		
		// Footer
		$this->load->view('footer');
		
	}
	
	public function score_save()
	{
		// Check input
		$ronde = $this->session->userdata('ronde');
		$lijstid = $this->session->userdata('lijstid');
		$subgroepid = $this->security->xss_clean($this->input->post('subgroepid'));
		
		// Score items ophalen
		$this->load->model('Beoordelen_model');
		$vragen = $this->Beoordelen_model->get_vragen();
		
		// Op te slaan data klaarzetten
		
		$i = 0;
		foreach ($vragen as $vraag)
		{
			$score[$i]['vraagid'] = $vraag['id'];
			
			// Check for type of awnser
			if ($vraag['antwoord'] == 0) {
				if ($this->input->post($vraag['id']) == 1) {
					$score[$i]['score'] = $vraag['score'];
				} else {
					$score[$i]['score'] = 0;
				}
			} elseif ($vraag['antwoord'] == 1) {
				$score[$i]['score'] = $this->input->post($vraag['id']);
			} elseif ($vraag['antwoord'] == 2) {
				$score[$i]['score'] = $this->input->post($vraag['id']);
			}
			$i++;
			
		}
		
		// Data opslaan
		$this->Beoordelen_model->store_score($ronde, $subgroepid, $score);
		
		// Redirecten naar nieuwe ploeg
		redirect(base_url().'beoordelen/subgroep');
	}
}