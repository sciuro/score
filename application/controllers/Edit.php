<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Info Class
*/
class Edit extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        if(! $this->session->userdata('validated')){
            redirect(base_url('/login'));
        }
		if (! ($this->session->admin == '1' OR $this->session->superadmin == '1')) {
			redirect(base_url('/dashboard'));
		}
	}
	
	public function index()
	{
		$data['page'] = 'edit';
		
		// Get data
		$this->load->model('Edit_model');
		$lijsten = $this->Edit_model->get_lijst();
		
		// Prepare data
		$data['lijsten'] = $lijsten;
		
		// Header
		$this->load->view('header', $data);
		
		// lijst list page
		$this->load->view('edit_lijst', $data);
		
		// Footer
		$this->load->view('footer');
	}
	
	public function enable($lijstid)
	{
		$this->load->model('Edit_model');
		$this->Edit_model->change_lijst($lijstid, '1');
		
		redirect(base_url('edit/'));
	}
	
	public function disable($lijstid)
	{
		$this->load->model('Edit_model');
		$this->Edit_model->change_lijst($lijstid, '0');
		
		redirect(base_url('edit/'));
	}
	
	public function lijst($lijstid, $ronde)
	{
		$data['page'] = 'edit';
		$score = array();
		
		// Get data
		$this->load->model('Edit_model');
		$subgroepen = $this->Edit_model->get_subgroepen();
		$vragen = $this->Edit_model->get_vragen($lijstid);
		$lijstnaam = $this->Edit_model->get_lijstnaam($lijstid);
		
		foreach ($vragen as $vraag) {
			$scorepv = $this->Edit_model->get_score($vraag['id'], $ronde);
			foreach ($scorepv as $item) {
				$score[$item['subgroepid']][$vraag['id']]['resultaatid'] = $item['id'];
				$score[$item['subgroepid']][$vraag['id']]['score'] = $item['score'];
			}
		}
		
		// Prepare data
		$data['subgroepen'] = $subgroepen;
		$data['vragen'] = $vragen;
		$data['scores'] = $score;
		$data['lijstid'] = $lijstid;
		$data['lijstnaam'] = $lijstnaam;
		$data['ronde'] = $ronde;
		
		// Header
		$this->load->view('header', $data);
		
		// lijst list page
		$this->load->view('edit_score', $data);
		
		// Footer
		$this->load->view('footer');
	}
	
	public function save()
	{
		if (!$this->input->post('save')) {
			redirect(base_url('edit/'));
		}
		
		// Get data
		$lijstid = $this->input->post('lijstid');
		
		$this->load->model('Edit_model');
		$subgroepen = $this->Edit_model->get_subgroepen();
		$vragen = $this->Edit_model->get_vragen($lijstid);
		
		$i=0;
		foreach ($subgroepen as $subgroep) {
			foreach ($vragen as $vraag) {
				if (!empty($this->input->post($vraag['id'].'-'.$subgroep['id']))) {
					$score=$this->input->post($vraag['id'].'-'.$subgroep['id']);
				} else {
					$score="0.00";
				}
				$store[$i] = array(
					'regioid' => $this->session->regio,
					'vraagid' => $vraag['id'],
					'ronde' => $this->input->post('ronde'),
					'subgroepid' => $subgroep['id'],
					'timestamp' => date("Y-m-d H:i:s"),
					'score' => $score,
					'userid' => $this->session->id,
				);
				$i++;
			}
		}	
	
		$this->Edit_model->save_score($store);
		
		redirect(base_url('edit/'));
	}
	
}