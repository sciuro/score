<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Info Class
*/
class Uitslag extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        if(! $this->session->userdata('validated')){
            redirect(base_url('/login'));
        }
		if (! ($this->session->admin == '1' OR $this->session->superadmin == '1')) {
			redirect(base_url('/dashboard'));
		}
	}
	
	public function index()
	{
		$data['page'] = 'uitslag';
		
		// Get data
		$this->load->model('Uitslag_model');
		$subgroepen = $this->Uitslag_model->get_subgroep_list();
		$spelgebieden = $this->Uitslag_model->get_spelgebieden_list();
		$maxpuntenraw = $this->Uitslag_model->get_max_punten();
		$maxtotaalpunten = $this->Uitslag_model->get_config('maxpunten');
		$data['round'] = $this->Uitslag_model->get_config('round');
		
		// Totaal bepalen
		$totaal = 0;
		$totaalgewicht = 0;
		foreach ($spelgebieden as $spelgebied) {
			$scoremax = $this->Uitslag_model->get_max_punten_detail($spelgebied['id']);
			$totaalgewicht = $totaalgewicht + $scoremax['gewicht'];
		}
		$totaalpsg = $maxtotaalpunten/$totaalgewicht;
		
		foreach ($maxpuntenraw as $max) {
			$maxpunten[$max['spelgebiedid']]['maxscore'] = $max['maxscore'];
			$maxpunten[$max['spelgebiedid']]['naam'] = $max['naam'];
			$maxpunten[$max['spelgebiedid']]['gewicht'] = $max['gewicht'];
		}
		
		$uitslagr = array();
		$uitslag = array();
		
		foreach ($subgroepen as $subgroep) {
			$uitslagr[$subgroep['id']]['total'] = 0;
			$scores = $this->Uitslag_model->get_subgroep_punten($subgroep['id']);
			
			foreach ($scores as $score) {
				$scoremaxsg = $this->Uitslag_model->get_max_punten_detail($score['spelgebiedid']);
				$scoreitem = ($score['score']/$scoremaxsg['maxscore']) * $totaalpsg * $scoremaxsg['gewicht'];
				$uitslagr[$subgroep['id']][$score['spelgebiedid']] = $scoreitem;
				$uitslagr[$subgroep['id']]['total'] = $uitslagr[$subgroep['id']]['total'] + $scoreitem;
			}
		}

		$uitslagorder = array();
 		foreach ($uitslagr as $key=>$val) {
		    $uitslagorder[$key] = $val['total'];
		}
		
		arsort($uitslagorder);
		$i = 1;
		foreach ($uitslagorder as $key=>$val) {
			$uitslag[$key] = $uitslagr[$key];
			$uitslag[$key]['rang'] = $i;
			$i++;
		}
		
		foreach ($subgroepen as $subgroep) {
			if (!isset($uitslag[$subgroep['id']]['rang'])) {
				$uitslag[$subgroep['id']]['rang'] = 999;
				$uitslag[$subgroep['id']] = 0;
			}
		}

		// Prepare data
		$data['subgroepen'] = $subgroepen;
		$data['spelgebieden'] = $spelgebieden;
		$data['uitslag'] = $uitslag;
		
		// Header
		$this->load->view('header', $data);
		
		// Ranking page
		$this->load->view('uitslag_ranking', $data);
		
		// Footer
		$this->load->view('footer');
	}
	
	public function groep($subgroepid)
	{
		$data['page'] = 'uitslag';
		
		// Get data
		$this->load->model('Uitslag_model');
		$onderdelen = $this->Uitslag_model->get_lijst_detail();
		$spelgebieden = $this->Uitslag_model->get_spelgebieden_list();
		$data['subgroepinfo'] = $this->Uitslag_model->get_subgroep($subgroepid);
		$maxtotaalpunten = $this->Uitslag_model->get_config('maxpunten');
		$data['round'] = $this->Uitslag_model->get_config('round');
				
		// Data order
		$totaal = 0;
		$totaalgewicht = 0;
		foreach ($spelgebieden as $spelgebied) {
			$scoremax = $this->Uitslag_model->get_max_punten_detail($spelgebied['id']);
			$totaalgewicht = $totaalgewicht + $scoremax['gewicht'];
		}
		$totaalpsg = $maxtotaalpunten/$totaalgewicht;
		
		foreach ($onderdelen as $onderdeel) {
			$scoremax = $this->Uitslag_model->get_max_punten_detail($onderdeel['spelgebiedid'], $onderdeel['onderdeelid']);
			$scoremaxsg = $this->Uitslag_model->get_max_punten_detail($onderdeel['spelgebiedid']);
			$score = $this->Uitslag_model->get_punten_detail($subgroepid, $onderdeel['spelgebiedid'], $onderdeel['onderdeelid']);

			$data['scores'][$onderdeel['spelgebiedid']]['naam'] = $onderdeel['spelgebied'];
			$data['scores'][$onderdeel['spelgebiedid']]['onderdeel'][$onderdeel['onderdeelid']]['naam'] = $onderdeel['onderdeel'];
			$data['scores'][$onderdeel['spelgebiedid']]['onderdeel'][$onderdeel['onderdeelid']]['scoremax'] = ($scoremax['maxscore']/$scoremaxsg['maxscore']) * $totaalpsg * $scoremax['gewicht'];
			$data['scores'][$onderdeel['spelgebiedid']]['onderdeel'][$onderdeel['onderdeelid']]['score'] = ($score['score']/$scoremaxsg['maxscore']) * $totaalpsg * $scoremax['gewicht'];
		}

		// Header
		$this->load->view('header', $data);
		
		// Ranking page
		$this->load->view('uitslag_groep', $data);
		
		// Footer
		$this->load->view('footer');
	}

}