<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Logout Class
*/
class Dashboard extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        if(! $this->session->userdata('validated')){
            redirect(base_url('/login'));
        }
	}
	
	public function index()
	{
		if ($this->session->admin == '1' OR $this->session->superadmin == '1') {
			redirect(base_url('/dashboard/admin'));
		} else {
			redirect(base_url('/dashboard/admin'));
			// redirect(base_url('/dashboard/user'));
		}
	}
	
	public function	admin()
	{
		$data['page'] = 'home';
		
		// Load model
		// Totaal bar
		$this->load->model('Dashboard_model');
		$vragen = $this->Dashboard_model->get_total_vragen();
		$antwoorden = $this->Dashboard_model->get_total_gedaan();
		
		if ($vragen == 0) {
			$data['gedaan'] = 0;
		} else {
			$data['gedaan'] = round(($antwoorden/$vragen)*100);
		}
		
		// Per lijst
		$subgroepen = $this->Dashboard_model->get_subgroepen();
		$lijsten = $this->Dashboard_model->get_lijsten();

		$bardata = array();

		foreach ($lijsten as $lijst)
		{
			$beantwoord = 0;
			$bardata[$lijst['id']]['naam'] = $lijst['naam'];
			$vragen = $this->Dashboard_model->get_vragen($lijst['id']);
			foreach ($vragen as $vraag)
			{
				$aantal = $this->Dashboard_model->get_antwoorden($vraag['id'])/$lijst['ronde'];
				$beantwoord = $beantwoord + $aantal;
			}
			if (($subgroepen*count($vragen)) == 0) {
				$bardata[$lijst['id']]['percentage'] = 0;
			} else {
				$bardata[$lijst['id']]['percentage'] = (($beantwoord)/($subgroepen*count($vragen)))*100;
			}
		}

		$data['bardata'] = $bardata;
		
		$barok = 0;
		$barnok = 0;
		$barbezig = 0;
		
		foreach ($bardata as $bar)
		{
			if ($bar['percentage'] == 100) {
				$barok = $barok + 1;
			} elseif ($bar['percentage'] == 0) {
				$barnok = $barnok + 1;
			} else {
				$barbezig = $barbezig + 1;
			}
		}
		
		if (($barok + $barnok + $barbezig) == 0) {
			$data['bar']['gedaan'] = 0;
			$data['bar']['bezig'] = 0;
			$data['bar']['doen'] = 100;
		} else {
			$data['bar']['gedaan'] = round(($barok/($barok + $barnok + $barbezig))*100);
			$data['bar']['bezig'] = round(($barbezig/($barok + $barnok + $barbezig))*100);
			$data['bar']['doen'] = 100-$data['bar']['gedaan']-$data['bar']['bezig'];
		}

		// Header
		$this->load->view('header', $data);
		
		// login page
		$this->load->view('dashboard_user', $data);
		
		// Footer
		$this->load->view('footer');
	}
	
	public function	user()
	{
		$data['page'] = 'home';
		
		// Load model
		$this->load->model('Dashboard_model');
		$vragen = $this->Dashboard_model->get_total_vragen();
		$antwoorden = $this->Dashboard_model->get_total_gedaan();
		
		$data['gedaan'] = round(($antwoorden/$vragen)*100);
		
		// Header
		$this->load->view('header', $data);
		
		// login page
		$this->load->view('dashboard_user', $data);
		
		// Footer
		$this->load->view('footer');
	}
	
	public function	changeregio()
	{
		if (! $this->session->superadmin == '1') {
			redirect(base_url('/dashboard'));
		}
		
		// Set new regionaam in session
		if ($this->security->xss_clean($this->input->post('regioid'))) {
			$this->session->set_userdata('regio', $this->security->xss_clean($this->input->post('regioid')));
			// Load model
			$this->load->model('Regio_model');
			$regionaam = $this->Regio_model->get_regio($this->security->xss_clean($this->input->post('regioid')));
			$this->session->set_userdata('regionaam', $regionaam);
			redirect(base_url('/dashboard'));
		}
		
		$data['page'] = 'regio';
		
		// Load model
		$this->load->model('Regio_model');
		$regios = $this->Regio_model->get_regio_list();
		
		// Arrange data
		foreach ($regios as $regio) {
			$data['regio'][$regio['id']] = $regio['naam'];
		}
				
		// Header
		$this->load->view('header', $data);
		
		// login page
		$this->load->view('sa_changeregio', $data);
		
		// Footer
		$this->load->view('footer');
	}
	
}
