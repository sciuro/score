<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Regio model
*/
class Info_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_subgroep_list()
	{
		$this->db->select('id, naam, themanaam, nummer');
		$this->db->from('subgroep');
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('jaar', date('Y'));
		$this->db->order_by('nummer', 'ASC');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_subgroep_info($id)
	{
		$this->db->select('subgroep.naam AS naam, themanaam, nummer, groep.naam AS groepsnaam, groep.plaats');
		$this->db->from('subgroep');
		$this->db->where('subgroep.regioid', $this->session->regio);
		$this->db->where('jaar', date('Y'));
		$this->db->where('subgroep.id', $id);
		$this->db->join('groep', 'subgroep.groepid=groep.id');
		
		$this->db->order_by('nummer', 'ASC');
		
		$query = $this->db->get();
		
		return $query->row();
	}
	
}