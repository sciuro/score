<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Regio model
*/
class Beoordelen_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_lijst()
	{
		$this->db->select('id, naam');
		$this->db->from('lijst');
		$this->db->where('jaar', date('Y'));
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('actief', '1');
		$this->db->order_by('naam', 'ASC');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_rondes($lijstid)
	{
		$this->db->select('ronde');
		$this->db->from('lijst');
		$this->db->where('id', $lijstid);
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('jaar', date('Y'));
		$this->db->where('actief', '1');
		
		$query = $this->db->get();
		
		return $query->row(); 
	}
	
	public function get_subgroepen()
	{
		$this->db->select('id, naam, themanaam, nummer');
		$this->db->from('subgroep');
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('jaar', date('Y'));
		$this->db->order_by('nummer', 'ASC');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_vragen()
	{
		$this->db->select('id, score, vraag, antwoord, score');
		$this->db->from('vragen');
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('lijstid', $this->session->lijstid);
		$this->db->where('jaar', date('Y'));
		
		$this->db->order_by('volgorde', 'ASC');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function check_score($ronde, $lijstid, $subgroepid)
	{
		$this->db->from('resultaat');
		$this->db->where('resultaat.regioid', $this->session->regio);
		$this->db->where('resultaat.ronde', $ronde);
		$this->db->where('vragen.lijstid', $lijstid);
		$this->db->where('resultaat.subgroepid', $subgroepid);
		$this->db->join('vragen', 'resultaat.vraagid=vragen.id');
		
		$query = $this->db->get();
		
		if ($query->num_rows() == 0)
		{
			return false;
		} else {
			return true;
		}
	}
	
	public function store_score($ronde, $subgroepid, $score)
	{
		foreach ($score as $item)
		{
			$data = array(
				'regioid' => $this->session->regio,
				'vraagid' => $item['vraagid'],
				'ronde' => $ronde,
				'subgroepid' => $subgroepid,
				'timestamp' => date("Y-m-d H:i:s"),
				'score' => $item['score'],
				'userid' => $this->session->id,
			);
			$this->db->insert('resultaat', $data);

		}
	}
}