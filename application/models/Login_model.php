<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Regio model
*/
class Login_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function check_user()
	{
		// Validate POST
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));
		$regioid = $this->security->xss_clean($this->input->post('regioid'));
		
		// Search for user
		$this->db->select('user.id, regioid, regio.naam AS regionaam, admin, superadmin');
		$this->db->where('username', $username);
		$this->db->where('password', sha1($password));
		$this->db->where('regioid', $regioid);
		$this->db->join('regio', 'user.regioid=regio.id');
		$this->db->from('user');
		
		$query = $this->db->get();

		if ($query->num_rows() == 1)
		{
			$row = $query->row();
			
			$userdata = array(
				'id' => $row->id,
				'regio' => $row->regioid,
				'regionaam' => $row->regionaam,
				'admin' => $row->admin,
				'superadmin' => $row->superadmin,
				'validated' => true
				);
			
			// Sent user information
			return $userdata;
		} else {
			return FALSE;
		}

	}
}