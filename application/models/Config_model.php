<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Regio model
*/
class Config_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_user_list()
	{
		$this->db->select('id, username, admin, superadmin');
		$this->db->from('user');
		$this->db->where('regioid', $this->session->regio);
		$this->db->order_by('username', 'ASC');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function update_user_rights($users)
	{
		foreach ($users as $id=>$user)
		{
			$this->db->where('id', $id);
			$this->db->where('regioid', $this->session->regio);
			$this->db->update('user', $user);
		}
	}
	
	public function add_user($user)
	{
		$this->db->insert('user', $user);
	}
	
	public function remove_user($id)
	{
		$this->db->where('id', $id);
		if ($this->session->superadmin != '1') {
			$this->db->where('superadmin !=', '1');
		}
		$this->db->delete('user');
	}
}