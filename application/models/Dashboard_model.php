<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Regio model
*/
class Dashboard_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_total_vragen()
	{
		$this->db->select('COUNT(*) as antwoord');
		$this->db->from('vragen');
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('jaar', date('Y'));
		
		$query = $this->db->get();
		
		$vragen = $query->row()->antwoord;
		
		$this->db->select('COUNT(*) as antwoord');
		$this->db->from('subgroep');
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('jaar', date('Y'));
		
		$query = $this->db->get();
		
		$subgroepen = $query->row()->antwoord;
		
		return $vragen*$subgroepen;
	}
	
	public function get_total_gedaan()
	{
		$this->db->select('COUNT(*) as antwoord');
		$this->db->from('resultaat');
		$this->db->where('regioid', $this->session->regio);
		//$this->db->where('jaar', date('Y'));
		
		$query = $this->db->get();
		
		return $query->row()->antwoord;
	}
	
	public function get_subgroepen()
	{
		$this->db->select('COUNT(*) as antwoord');
		$this->db->from('subgroep');
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('jaar', date('Y'));
		
		$query = $this->db->get();
		
		return $query->row()->antwoord;
	}
	
	public function get_lijsten()
	{
		$this->db->select('id, naam, ronde');
		$this->db->from('lijst');
		$this->db->where('jaar', date('Y'));
		$this->db->where('regioid', $this->session->regio);
		$this->db->order_by('naam', 'ASC');
				
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_vragen($lijstid)
	{
		$this->db->select('id');
		$this->db->from('vragen');
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('jaar', date('Y'));
		$this->db->where('lijstid', $lijstid);
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_antwoorden($vraagid)
	{
		$this->db->select('COUNT(*) as antwoord');
		$this->db->from('resultaat');
		$this->db->where('regioid', $this->session->regio);
		//$this->db->where('jaar', date('Y'));
		$this->db->where('vraagid', $vraagid);
		
		$query = $this->db->get();
		
		return $query->row()->antwoord;
	}
}