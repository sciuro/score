<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Regio model
*/
class Uitslag_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_subgroep_list()
	{
		$this->db->select('subgroep.id AS id,
			subgroep.naam AS naam,
			subgroep.themanaam AS themanaam,
			subgroep.nummer AS nummer,
			groep.naam AS groepsnaam,
			groep.plaats AS plaats');
		$this->db->from('subgroep');
		$this->db->where('subgroep.regioid', $this->session->regio);
		$this->db->where('subgroep.jaar', date('Y'));
		$this->db->join('groep', 'subgroep.groepid=groep.id', 'left');
		$this->db->order_by('nummer', 'ASC');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_subgroep($subgroepid)
	{
		$this->db->select('subgroep.id AS id,
			subgroep.naam AS naam,
			subgroep.themanaam AS themanaam,
			subgroep.nummer AS nummer,
			groep.naam AS groepsnaam,
			groep.plaats AS plaats');
		$this->db->from('subgroep');
		$this->db->where('subgroep.regioid', $this->session->regio);
		$this->db->where('subgroep.jaar', date('Y'));
		$this->db->where('subgroep.id', $subgroepid);
		$this->db->join('groep', 'subgroep.groepid=groep.id', 'left');
		
		$query = $this->db->get();
		
		return $query->row_array();
	}
	
	public function get_spelgebieden_list()
	{
		$this->db->select('id, naam');
		$this->db->from('spelgebied');
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('jaar', date('Y'));
		$this->db->order_by('naam', 'ASC');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_config($configname)
	{
		$this->db->select('value');
		$this->db->from('config');
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('jaar', date('Y'));
		$this->db->where('name', $configname);
		
		$query = $this->db->get();
		
		return $query->row()->value;
	}
	
	public function get_subgroep_punten($subgroepid)
	{
		$this->db->select('SUM('.$this->db->dbprefix('resultaat').'.score/'.$this->db->dbprefix('lijst').'.ronde) AS score,
			spelgebied.id AS spelgebiedid,
			');
		$this->db->from('resultaat');
		
		$this->db->where('resultaat.regioid', $this->session->regio);
		$this->db->where('vragen.jaar', date('Y'));
		$this->db->where('resultaat.subgroepid', $subgroepid);
		
		$this->db->join('vragen', 'resultaat.vraagid=vragen.id', 'left');
		$this->db->join('onderdeel', 'vragen.onderdeelid=onderdeel.id', 'left');
		$this->db->join('spelgebied', 'onderdeel.spelgebiedid=spelgebied.id', 'left');
		$this->db->join('lijst', 'vragen.lijstid=lijst.id', 'left');
		
		$this->db->group_by('spelgebied.naam');
		
		$this->db->order_by('spelgebied.id', 'ASC');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_max_punten()
	{
		$this->db->select('SUM('.$this->db->dbprefix('vragen').'.score) AS maxscore, 
			spelgebied.naam AS naam,
			spelgebied.id AS spelgebiedid,
			spelgebied.gewicht AS gewicht');
		$this->db->from('vragen');
		
		$this->db->where('vragen.regioid', $this->session->regio);
		$this->db->where('vragen.jaar', date('Y'));
		
		$this->db->join('onderdeel', 'vragen.onderdeelid=onderdeel.id', 'left');
		$this->db->join('spelgebied', 'onderdeel.spelgebiedid=spelgebied.id', 'left');
		
		$this->db->group_by('spelgebied.naam');
		
		$this->db->order_by('spelgebied.id', 'ASC');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_lijst_detail()
	{
		$this->db->select('spelgebied.naam AS spelgebied,
			spelgebied.id AS spelgebiedid,
			onderdeel.naam AS onderdeel,
			onderdeel.id AS onderdeelid
			');
		$this->db->from('onderdeel');
		
		$this->db->where('onderdeel.regioid', $this->session->regio);
		$this->db->where('onderdeel.jaar', date('Y'));
		
		$this->db->join('spelgebied', 'onderdeel.spelgebiedid=spelgebied.id', 'left');
		
		// $this->db->group_by('spelgebied.id, onderdeel.id');
		
		$this->db->order_by('spelgebied.naam, onderdeel.naam', 'ASC');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_punten_detail($subgroepid, $spelgebiedid, $onderdeelid)
	{
		$this->db->select('SUM('.$this->db->dbprefix('resultaat').'.score/'.$this->db->dbprefix('lijst').'.ronde) AS score,
			spelgebied.naam AS spelgebied,
			spelgebied.id AS spelgebiedid,
			onderdeel.naam AS onderdeel,
			onderdeel.id AS onderdeelid
			');
		$this->db->from('resultaat');
		
		$this->db->where('resultaat.regioid', $this->session->regio);
		$this->db->where('vragen.jaar', date('Y'));
		$this->db->where('resultaat.subgroepid', $subgroepid);
		$this->db->where('vragen.onderdeelid', $onderdeelid);
		$this->db->where('onderdeel.spelgebiedid', $spelgebiedid);
		
		$this->db->join('vragen', 'resultaat.vraagid=vragen.id', 'left');
		$this->db->join('onderdeel', 'vragen.onderdeelid=onderdeel.id', 'left');
		$this->db->join('spelgebied', 'onderdeel.spelgebiedid=spelgebied.id', 'left');
		$this->db->join('lijst', 'vragen.lijstid=lijst.id', 'left');
		
		$this->db->group_by('vragen.onderdeelid');
		
		$this->db->order_by('spelgebied.naam, onderdeel.naam', 'ASC');
		
		$query = $this->db->get();
		
		return $query->row_array();
	}
	
	public function get_max_punten_detail($spelgebiedid, $onderdeelid = NULL)
	{
		$this->db->select('SUM('.$this->db->dbprefix('vragen').'.score) AS maxscore,
			spelgebied.gewicht AS gewicht');
		$this->db->from('vragen');
		
		$this->db->where('vragen.regioid', $this->session->regio);
		$this->db->where('vragen.jaar', date('Y'));
		if (!empty($onderdeelid)) {
			$this->db->where('vragen.onderdeelid', $onderdeelid);
		}
		$this->db->where('onderdeel.spelgebiedid', $spelgebiedid);
		
		$this->db->join('onderdeel', 'vragen.onderdeelid=onderdeel.id', 'left');
		$this->db->join('spelgebied', 'onderdeel.spelgebiedid=spelgebied.id', 'left');
		
		if (!empty($onderdeelid)) {
			$this->db->group_by('spelgebied.id, onderdeel.id');
		} else {
			$this->db->group_by('spelgebied.id');
		}
		
		$this->db->order_by('spelgebied.id, onderdeel.id', 'ASC');
		
		$query = $this->db->get();
		
		return $query->row_array();
	}
}