<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Regio model
*/
class Regio_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_regio_list()
	{
		$this->db->select('id, naam');
		$this->db->from('regio');
		$this->db->order_by('naam', 'ASC');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_regio($id)
	{
		$this->db->select('naam');
		$this->db->from('regio');
		$this->db->where('id', $id);
		
		$query = $this->db->get();
		
		$return = $query->row();
		
		return $return->naam;
	}
}