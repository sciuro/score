<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Regio model
*/
class Edit_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_lijst()
	{
		$this->db->select('id, naam, ronde, actief');
		$this->db->from('lijst');
		$this->db->where('jaar', date('Y'));
		$this->db->where('regioid', $this->session->regio);
		$this->db->order_by('naam', 'ASC');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_lijstnaam($id)
	{
		$this->db->select('naam');
		$this->db->from('lijst');
		$this->db->where('jaar', date('Y'));
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('id', $id);
		$this->db->order_by('naam', 'ASC');
		
		$query = $this->db->get();
		
		return $query->row()->naam;
	}
	
	public function change_lijst($id, $val)
	{
		$data = array(
			'actief' => $val
		);
		$this->db->where('id', $id);
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('jaar', date('Y'));
		$this->db->update('lijst', $data);
	}
	
	public function get_subgroepen()
	{
		$this->db->select('id, naam, themanaam, nummer');
		$this->db->from('subgroep');
		$this->db->where('subgroep.regioid', $this->session->regio);
		$this->db->where('subgroep.jaar', date('Y'));
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_vragen($lijstid)
	{
		$this->db->select('vragen.id AS id, vraag, antwoord, score');
		$this->db->from('vragen');
		$this->db->where('vragen.regioid', $this->session->regio);
		$this->db->where('vragen.jaar', date('Y'));
		$this->db->where('lijst.id', $lijstid);
		
		$this->db->join('lijst', 'vragen.lijstid=lijst.id', 'left');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function get_score($vraagid, $ronde)
	{
		$this->db->select('id, subgroepid, score');
		$this->db->from('resultaat');
		$this->db->where('regioid', $this->session->regio);
		$this->db->where('vraagid', $vraagid);
		$this->db->where('ronde', $ronde);
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function save_score($data)
	{
		foreach ($data as $row) {
			// Check for insert or update
			$this->db->select('COUNT(*) AS aantal');
			$this->db->from('resultaat');
			$this->db->where('regioid', $this->session->regio);
			$this->db->where('vraagid', $row['vraagid']);
			$this->db->where('subgroepid', $row['subgroepid']);
			$this->db->where('ronde', $row['ronde']);
			$query = $this->db->get();
			$rows = $query->row()->aantal;
						
			if ($rows==1) {
				$this->db->where('regioid', $this->session->regio);
				$this->db->where('vraagid', $row['vraagid']);
				$this->db->where('subgroepid', $row['subgroepid']);
				$this->db->where('ronde', $row['ronde']);
				$this->db->update('resultaat', $row);
			} elseif ($rows==0) {
				$this->db->insert('resultaat', $row);
			} else {
				echo "Duplicate result!";
			}
		}
	}
	
}