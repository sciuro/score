<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Add regio table
*/
class Migration_Add_user extends CI_Migration
{
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => '4',
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'regioid' => array(
				'type' => 'INT',
				'constraint' => '3',
				'unsigned' => TRUE,
			),
			'username' => array(
				'type' => 'VARCHAR',
				'constraint' => '80',
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '80',
			),
			'admin' => array(
				'type' => 'INT',
				'constraint' => '1',
				'unsigned' => TRUE,
				'default' => '0',
			),
			'superadmin' => array(
				'type' => 'INT',
				'constraint' => '1',
				'unsigned' => TRUE,
				'default' => '0',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('user');

	}
	
	public function	down()
	{
		$this->dbforge->drop_table('user');
	}
}