<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Add regio table
*/
class Migration_Add_resultaat extends CI_Migration
{
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => '6',
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'regioid' => array(
				'type' => 'INT',
				'constraint' => '3',
				'unsigned' => TRUE,
			),
			'vraagid' => array(
				'type' => 'INT',
				'constraint' => '6',
				'unsigned' => TRUE,
			),
			'ronde' => array(
				'type' => 'INT',
				'constraint' => '1',
				'unsigned' => TRUE,
			),
			'subgroepid' => array(
				'type' => 'INT',
				'constraint' => '5',
			),
			'timestamp' => array(
				'type' => 'DATETIME',
			),
			'score' => array(
				'type' => 'INT',
				'constraint' => '6',
				'unsigned' => TRUE,
			),
			'userid' => array(
				'type' => 'INT',
				'constraint' => '4',
			),
			'lokatie' => array(
				'type' => 'VARCHAR',
				'constraint' => '80',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('resultaat');

	}
	
	public function	down()
	{
		$this->dbforge->drop_table('resultaat');
	}
}