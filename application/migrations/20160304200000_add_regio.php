<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Add regio table
*/
class Migration_Add_regio extends CI_Migration
{
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => '3',
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'naam' => array(
				'type' => 'VARCHAR',
				'constraint' => '80',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('regio');
		
		$data = array(
		        'id' => '0',
		        'naam' => 'LSW'
		);

		$this->db->insert('regio', $data);
	}
	
	public function	down()
	{
		$this->dbforge->drop_table('regio');
	}
}