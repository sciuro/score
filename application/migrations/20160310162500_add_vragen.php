<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Add regio table
*/
class Migration_Add_vragen extends CI_Migration
{
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => '6',
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'regioid' => array(
				'type' => 'INT',
				'constraint' => '3',
				'unsigned' => TRUE,
			),
			'onderdeelid' => array(
				'type' => 'INT',
				'constraint' => '6',
				'unsigned' => TRUE,
			),
			'lijstid' => array(
				'type' => 'INT',
				'constraint' => '6',
				'unsigned' => TRUE,
			),
			'volgorde' => array(
				'type' => 'INT',
				'constraint' => '6',
				'unsigned' => TRUE,
			),
			'jaar' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
				'constraint' => '4',
			),
			'vraag' => array(
				'type' => 'VARCHAR',
				'constraint' => '80',
			),
			'antwoord' => array(
				'type' => 'INT',
				'constraint' => '1',
				'unsigned' => TRUE,
			),
			'score' => array(
				'type' => 'INT',
				'constraint' => '6',
				'unsigned' => TRUE,
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('vragen');

	}
	
	public function	down()
	{
		$this->dbforge->drop_table('vragen');
	}
}