<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Add regio table
*/
class Migration_Add_spelgebiedweight extends CI_Migration
{
	
	public function up()
	{
		$this->dbforge->add_column('spelgebied', array(
			'gewicht' => array(
				'type' => 'DECIMAL',
				'unsigned' => TRUE,
				'constraint' => '3,2',
				'after' => 'jaar',
				'default' => '1.00',
			),
		));

	}
	
	public function	down()
	{
		$this->dbforge->drop_column('spelgebied', 'gewicht');
	}
}