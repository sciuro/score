<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Add regio table
*/
class Migration_Add_subgroep extends CI_Migration
{
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => '5',
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'regioid' => array(
				'type' => 'INT',
				'constraint' => '3',
				'unsigned' => TRUE,
			),
			'groepid' => array(
				'type' => 'INT',
				'constraint' => '5',
				'unsigned' => TRUE,
			),
			'jaar' => array(
				'type' => 'INT',
				'constraint' => '4',
				'unsigned' => TRUE,
			),
			'naam' => array(
				'type' => 'VARCHAR',
				'constraint' => '80',
			),
			'themanaam' => array(
				'type' => 'VARCHAR',
				'constraint' => '80',
			),
			'nummer' => array(
				'type' => 'VARCHAR',
				'constraint' => '80',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('subgroep');

	}
	
	public function	down()
	{
		$this->dbforge->drop_table('subgroep');
	}
}