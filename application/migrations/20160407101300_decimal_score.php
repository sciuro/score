<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Add regio table
*/
class Migration_Decimal_score extends CI_Migration
{
	
	public function up()
	{
		$this->dbforge->modify_column('vragen', array(
			'score' => array(
				'type' => 'DECIMAL',
				'constraint' => '4,2',
				'unsigned' => TRUE,
			),
		));
		
		$this->dbforge->modify_column('resultaat', array(
			'score' => array(
				'type' => 'DECIMAL',
				'constraint' => '4,2',
				'unsigned' => TRUE,
			),
		));

	}
	
	public function	down()
	{
		$this->dbforge->modify_column('vragen', array(
			'score' => array(
				'type' => 'INT',
				'constraint' => '6',
				'unsigned' => TRUE,
			),
		));
		
		$this->dbforge->modify_column('resultaat', array(
			'score' => array(
				'type' => 'INT',
				'constraint' => '6',
				'unsigned' => TRUE,
			),
		));
		
	}
}