# README #

This is a score system for scout patrols in the Netherlands.

### Installation ###

* Copy the whole tree to a webserver with php support.
* In /application/config/ copy config_example.php to config.php.
* In /application/config/ copy database_example.php to database.php.
* Edit the config.php and edit the variables:
	 * base_url, the URL of the website.
	 * language, your language. Dutch or English.
	 * sess_*, your session cookie preferences.
* Edit the database.php to your needs.
